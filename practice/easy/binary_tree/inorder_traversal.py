
# ? Given the root of a binary tree, return the inorder traversal of its nodes' values.

# ?  
# ? Example 1:
# ? Input: root = [1,null,2,3]
# ? Output: [1,3,2]

# ? Example 2:
# ? Input: root = []
# ? Output: []

# ? Example 3:
# ? Input: root = [1]
# ? Output: [1]
# ?  

# ? Constraints:
# ?     The number of nodes in the tree is in the range [0, 100].
# ?     -100 <= Node.val <= 100
# ? Follow up: Recursive solution is trivial, could you do it iteratively?


# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

# ? recursive solution
class Solution(object):
    def inorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        res = []

        def inorder(root):
            if not root:
                return
            inorder(root.left)
            res.append(root.val)
            inorder(root.right)
        inorder(root)
        return res


class Solution(object):  # ? iterative solution
    def inorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        res = []
        stack = []
        cur = root
        while cur or stack:
            while cur:
                stack.append(cur)
                cur = cur.left
            cur = stack.pop()
            res.append(cur.val)
            cur = cur.right
        return res


s = Solution()
a = [1, None, 2, 3]
b = s.inorderTraversal()
