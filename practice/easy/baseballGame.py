def calPoints(ops) -> int:
    """
    :type ops: List[str] - List of operations
    :rtype: int - Sum of scores after performing all operations
    """
    stack = ops
    for i, t in enumerate(ops):
        print(t)
        if t == "C":
            stack.pop(i-1)
        elif t == "D":
            stack.append(t * -2)
        elif t == "+":
            stack.append(t + stack[i-1])
        else:
            stack.append(t)
    return sum(stack)


if __name__ == '__main__':
    print(calPoints(["5", "2", "C", "D", "+"]))
