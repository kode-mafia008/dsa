from math import floor


class Solution(object):
    def mySqrt(self, x):
        """
        :type x: int
        :rtype: int
        """
        n = x
        x1 = x
        for _ in range(10, 0, -1):
            x1 = (x1 + n/x1)/2
        return x1


s = Solution()
a = s.mySqrt(12)
print(floor(a))
